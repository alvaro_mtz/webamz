<?php
/*
* Template Name: Home
*/
 
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );?>
 
<?php get_header();?>
<main class="main-content">

<div class="home_general">
    <div class="nombre">
        <p>Álvaro Martínez</p>
    </div>
    <div class="quehago">
        <p class="quehago_text">Web Developer en</p><a href="http://allwhite.es/"><img class="quehago_image" src="<?php echo get_template_directory_uri() . '/imagenes/logo_aw_allwhite-2.png'; ?>"alt=""> </a> <p class="quehago_text">AllWhite</p>
    </div>
</div>

</main>
<?php get_footer();?>
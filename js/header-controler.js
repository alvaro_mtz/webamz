$(window).on('load', function () {



  var lineDrawingIdeas = anime({
    targets: ' .path_ideas',
    strokeDashoffset: [anime.setDashoffset, 0],
    easing: 'linear',
    duration: 2000,

  });
  var lineDrawingInspiring = anime({
    targets: ' path_inspiring',
    strokeDashoffset: [anime.setDashoffset, 0],
    easing: 'linear',
    duration: 2000,

  });
  var lineDrawingOnda1 = anime({
    targets: ' .path_onda1',
    strokeDashoffset: [anime.setDashoffset, 0],
    easing: 'linear',
    duration: 5000,

  });
  var lineDrawingOnda2 = anime({
    targets: ' .path_onda2',
    strokeDashoffset: [anime.setDashoffset, 0],
    easing: 'linear',
    duration: 4000,

  });
  var lineDrawingOnda3 = anime({
    targets: ' .path_onda3',
    strokeDashoffset: [anime.setDashoffset, 0],
    easing: 'linear',
    duration: 3000,

  });
  var lineDrawingOnda4 = anime({
    targets: ' .path_onda4',
    strokeDashoffset: [anime.setDashoffset, 0],
    easing: 'linear',
    duration: 2000,

  });



  var pathred1 = anime.path('.path_floatred1');

  var motionPath = anime({
    targets: '.small',
    translateX: pathred1('x'),
    translateY: pathred1('y'),
    rotate: pathred1('angle'),
    easing: 'linear',
    duration: 10000,
    loop: true
  });
  var pathred2 = anime.path('.path_floatred2');

  var motionPath = anime({
    targets: '.small2',
    translateX: pathred2('x'),
    translateY: pathred2('y'),
    rotate: pathred2('angle'),
    easing: 'linear',
    duration: 20000,
    loop: true
  });
  var pathred3 = anime.path('.path_floatred3');

  var motionPath = anime({
    targets: '.small3',
    translateX: pathred3('x'),
    translateY: pathred3('y'),
    rotate: pathred3('angle'),
    easing: 'linear',
    duration: 10000,
    loop: true
  });



  var pathred4 = anime.path('.path_floatred4');

  var motionPath = anime({
    targets: '.small4',
    translateX: pathred4('x'),
    translateY: pathred4('y'),
    rotate: pathred4('angle'),
    easing: 'linear',
    duration: 15000,
    loop: true
  });

  var pathred5 = anime.path('.path_floatred5');

  var motionPath = anime({
    targets: '.small5',
    translateX: pathred5('x'),
    translateY: pathred5('y'),
    rotate: pathred5('angle'),
    easing: 'linear',
    duration: 10000,
    loop: true
  });

  var pathred6 = anime.path('.path_floatred6');

  var motionPath = anime({
    targets: '.small6',
    translateX: pathred6('x'),
    translateY: pathred6('y'),
    rotate: pathred6('angle'),
    easing: 'linear',
    duration: 8000,
    loop: true
  });

  var pathred7 = anime.path('.path_floatred7');

  var motionPath = anime({
    targets: '.small7',
    translateX: pathred7('x'),
    translateY: pathred7('y'),
    rotate: pathred7('angle'),
    easing: 'easeInQuad',
    duration: 28000,
    loop: true
  });

  var pathred8 = anime.path('.path_floatred8');

  var motionPath = anime({
    targets: '.small8',
    translateX: pathred8('x'),
    translateY: pathred8('y'),
    rotate: pathred8('angle'),
    easing: 'linear',
    duration: 8000,
    loop: true
  });

});
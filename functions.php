<?php
function my_theme_setup() { 
    // Switch default core markup for search form, comment form, and comments to output valid HTML5.
    add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );
    add_theme_support( 'post-thumbnails' );
}
add_action( 'after_setup_theme', 'my_theme_setup' );


function my_theme_cssjs() {
    wp_enqueue_script( 'my-theme-js1', get_template_directory_uri() . '/js/jquery-3.2.1.min.js', array( 'jquery' ), '', true );
    wp_register_script( 'Boostrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', null, null, true );
    wp_enqueue_script('Boostrap');
    Wp_register_style( 'Boostrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
    wp_enqueue_style('Boostrap');
    wp_enqueue_style( 'navbarall', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'homepage', get_template_directory_uri() . '/css/style.css' );
    wp_enqueue_script( 'my-theme-js4', get_template_directory_uri() . '/js/menucontroller.js', array( 'jquery' ), '', true );
    wp_enqueue_script( 'my-theme-js3', get_template_directory_uri() . '/js/header-controler.js', array( 'jquery' ), '', true );
    wp_enqueue_script( 'my-theme-js2', get_template_directory_uri() . '/js/anime.min.js', array( 'jquery' ), '', true );
}
add_action( 'wp_enqueue_scripts', 'my_theme_cssjs' );



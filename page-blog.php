<?php
/*
* Template Name: Blog
*/
 
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );?>
 
<?php get_header();?>
<div id="mainContent">
<?php $args = array('post_type' => 'Post'); ?>
    <?php $loop = new WP_Query($args); ?>
        <?php $posts=query_posts($query_string . "?order=acs"); if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
        <div class="post_loop">
            <p class="date"> <?php the_date() ?> </p>
            <a class="button" href="<?php echo get_permalink() ?>">
            <p class="title"> <?php the_title() ?> </p>
            </a>
        </div>
        <?php endwhile; ?>
        <?php else: ?>
            <h1>Algo no ha cargado bien, actualiza la página</h1>
        <?php endif; ?>
    <?php wp_reset_postdata(); ?>	
</div>
<?php get_footer();?>